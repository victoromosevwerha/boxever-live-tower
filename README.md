Boxever Live Ansible Tower Deployment

This is currently set up to run the tasks/vpc folder's playbooks

Launch by running 'sh build.sh' on the CLI

This runs the boxever-live-tower.yml playbook and deploys the following components

- Ansible Tower nodes in a cluster (without the Ansible Tower software)
- Bastion host

After installing these components, deploy the Ansible Tower software on the nodes as a clusterusing the relevant repository (currently saved at the following URL https://bitbucket.org/boxever/boxever-tower-install - assign the relevant variables in its variable locations for the VPC)