export RDSDATABASE=boxever-live-tower-live-rds.clmrzuolhcae.eu-west-1.rds.amazonaws.com
export IPADDRESSA=172.20.0.141
export IPADDRESSB=172.20.1.24
export IPADDRESSC=172.20.2.88
export RABBITA=$(echo $IPADDRESSA | sed s/\\./\-/g)
export RABBITB=$(echo $IPADDRESSB | sed s/\\./\-/g)
export RABBITC=$(echo $IPADDRESSC | sed s/\\./\-/g)
export TOWERUSERNAME=ec2-user
export TOWERKEYNAME=boxever-live-tower-key.pem

sudo yum -y install telnet
sudo yum -y install ansible
sudo yum -y install nano
sudo yum -y install python-pip
sudo yum -y install pip

curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"

sudo python get-pip.py
sudo pip install awscli

sleep 1

sudo pip install botocore
sudo pip install boto
sudo pip install boto3
sudo python-pip install botocore
sudo python-pip install boto
sudo python-pip install boto3

sleep 1

#Set up the directory structure

sudo mkdir /var
sudo mkdir/var/www
sudo mkdir /var/www/html
cd /var/www/html

sleep 1

#Exit and go back to the shell on the jump box and copy the public key to the primary Tower server
#sudo scp -i $TOWERKEYNAME centos@X.X.X.X:/var/www/html/tower

sudo curl -O -J https://releases.ansible.com/ansible-tower/setup-bundle/ansible-tower-setup-bundle-latest.el7.tar.gz
towerzip=$(ls | grep tower)
sudo tar xzf $towerzip
towerextract=$(ls | grep -v gz | grep -v py)
sudo mv $towerextract tower

cd /var/www/html/tower

sleep 1

cat > /var/www/html/tower/inventory_cluster <<EOF
[tower]
$IPADDRESSA rabbitmq_host=ip-$RABBITA ansible_ssh_user=$TOWERUSERNAME ansible_ssh_private_key_file="$TOWERKEYNAME"
$IPADDRESSB rabbitmq_host=ip-$RABBITB ansible_ssh_user=$TOWERUSERNAME ansible_ssh_private_key_file="$TOWERKEYNAME"
$IPADDRESSC rabbitmq_host=ip-$RABBITC ansible_ssh_user=$TOWERUSERNAME ansible_ssh_private_key_file="$TOWERKEYNAME"

[database]

[all:vars]

ansible_become=true
admin_password='Kalakuta01'

pg_host='$RDSDATABASE'
pg_port='5432'
pg_database='postgres'
pg_username='postgres'
pg_password='Kalakuta01'

#rabbitmq_port=5672
rabbitmq_vhost=tower
rabbitmq_username=tower
rabbitmq_password='Kalakuta01'
rabbitmq_cookie=cookiemonster

# Isolated Tower nodes automatically generate an RSA key for authentication;
# To disable this behavior, set this value to false

isolated_key_generation=true

EOF

cat > /var/www/html/tower/roles/nginx/tasks/tasks.yml <<EOF
---
- name: Determine if selinux is enabled
  command: getenforce
  register: getenforce
  ignore_errors: true
  changed_when: false

- name: Open up permissions on nginx.
  seboolean:
    name: '{{item}}'
    persistent: yes
    state: 'true'
#   when: getenforce is success and getenforce.stdout.lower() != "disabled"
  with_items:
    - httpd_can_network_connect

- name: create self signed SSL certificates
  command: openssl req -x509 -nodes -sha256 -days 99999 -newkey rsa:2048 -keyout /etc/tower/tower.key -out /etc/tower/tower.cert -subj "/CN=localhost"
  args:
    creates: /etc/tower/tower.cert
#   when: inventory_hostname == groups['tower'][0]
  notify:
    - restart nginx

- name: set permissions on SSL certificate
  file:
    path: '{{ item }}'
    group: '{{ aw_group }}'
    follow: yes
    mode: 0640
    owner: root
#   when: inventory_hostname == groups['tower'][0]
  with_items:
  - /etc/tower/tower.cert
  - /etc/tower/tower.key

- name: slurp self-signed SSL certificate
  slurp:
    src: /etc/tower/tower.cert
#  when: inventory_hostname == groups['tower'][0]
  register: tower_cert
  no_log: True

- name: slurp self-signed SSL key
  slurp:
    src: /etc/tower/tower.key
#   when: inventory_hostname == groups['tower'][0]
  register: tower_key
  no_log: True

#- name: place self-signed SSL certificates
#  copy:
#    content: "{{ hostvars[groups['tower'][0]]['tower_cert']['content'] | b64decode }}"
#    dest: /etc/tower/tower.cert
#    group: '{{ aw_group }}'
#    follow: yes
#    mode: 0640
#    force: false
#    owner: root
#  when: inventory_hostname != groups['tower'][0]
#  notify:
#    - restart nginx

#- name: place self-signed SSL key file
#  copy:
#    content: "{{ hostvars[groups['tower'][0]]['tower_key']['content'] | b64decode }}"
#    dest: /etc/tower/tower.key
#    group: '{{ aw_group }}'
#    follow: yes
#    mode: 0640
#    force: false
#    owner: root
#  when: inventory_hostname != groups['tower'][0]
#  notify:
#    - restart nginx

- name: Install Tower nginx.conf
  template:
    src: nginx.conf
    dest: '/etc/nginx/nginx.conf'
    force: True
    owner: root
    group: root
  notify:
    - restart nginx

- name: start nginx and configure to startup automatically
  service:
    name: nginx
    state: started
    enabled: yes
EOF

sleep 1

#ssh -i $TOWERKEYNAME $TOWERUSERNAME@$IPADDRESSA
#ssh -i $TOWERKEYNAME $TOWERUSERNAME@$IPADDRESSB
#ssh -i $TOWERKEYNAME $TOWERUSERNAME@$IPADDRESSC
#sudo chown $TOWERKEYNAME:$TOWERKEYNAME *.pem

sudo ./setup.sh -i inventory_cluster
